<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {
  public function __construct() {
		parent::__construct();
  }
  // @welcome
  public function index() {
    $data = array_merge(frontend_info());
		$this->parser->parse('welcome/index', $data);
	}
}
?>
