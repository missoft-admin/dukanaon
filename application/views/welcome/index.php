<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <!-- @error -->
    <link rel="stylesheet" href="{css_path}custom-style.css">

	<link href="{css_path}bootstrap-slider.css" rel="stylesheet">
	<link href="{css_path}font-awesome.css" rel="stylesheet">

	<!-- Custom Plugin -->
	<link href="{css_path}common.css" rel="stylesheet">
    <link href="{css_path}custom-select.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{css_path}km-custom-plugin/item-slider/slick.css">
	<link rel="stylesheet" type="text/css" href="{css_path}km-custom-plugin/item-slider/slick-theme.css">


	<!-- Script for Price Range Slider -->
	<script type="text/javascript" src="{js_path}jquery-1.11.1.js"></script>
	<script type="text/javascript" src="{js_path}bootstrap-slider.js"></script>
  <script type="text/javascript" src="{js_path}km-custom-plugin/jquery.selectbox.js"></script>


	<!-- Global Jquery Needed -->
	<script src="{js_path}modernizr.custom.63321.js"></script>
    <title>Kulcimart - Homepage</title>


	<!-- Script for Item Slider Start -->

	<!-- Script for Item Slider End -->
  </head>
  <body>
		<section class="header">
			<div class="row">
				<div class="col-sm-12">
					<nav class="navbar navbar-expand-lg navbar-light bg-light text-center">
						<div class="container">
						<form class="form-inline my-2 my-lg-0">
						  <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
						  <button class="btn  my-2 my-sm-0 btn-primary" type="submit">Search</button>
						</form>
						<!-- <a class="navbar-brand" href="#">Kulcimart</a> -->
						  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
							<span class="navbar-toggler-icon"></span>
						  </button>
						  <div class="collapse navbar-collapse" id="navbarSupportedContent">
							<ul class="navbar-nav mr-auto">
							  <li class="nav-item active">
								<a class="nav-link" href="#">Promo <span class="sr-only">(current)</span></a>
							  </li>
							  <li class="nav-item">
								<a class="nav-link" href="#">Event</a>
							  </li>
							  <li class="nav-item">
								<a class="nav-link" href="#">Berita</a>
							  </li>
							  <li class="nav-item">
								<a class="nav-link btn btn-success" href="#">Daftar</a>
							  </li>
							  <li class="nav-item">
								<a class="nav-link btn btn-info" href="#">Login</a>
							  </li>
							</ul>
						  </div>
					  </div>
					</nav>
				</div>
			</div>
			<div class="km-featured-section">
				<div class="container">
					<div class="row">
						<div class="col-sm-12 text-center">
							<img src="{img_path}header-logo.png"/>
							<h1>Selamat datang di Kulcimart.com</h1>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section class="content">
			<div class="wing-ads-left">
				<img src="{img_path}wing-ads-1.jpg"/>
				<img src="{img_path}wing-ads-2.jpg"/>
			</div>
			<div class="wing-ads-right">
				<img src="{img_path}wing-ads-3.jpg"/>
				<img src="{img_path}wing-ads-4.jpg"/>
			</div>
			<div class="container">

				<div class="km-filter-box">
					<div class="row">
						<div class="col-sm-12">
							<h3 class="text-center">Filter Pencarian</h3>
						</div>
						<div class="col-sm-4">
							<div class="form-group">
								<label for="exampleFormControlSelect1">Pilih Kota</label>
								<select id="select-city" class="cd-select">
								  <option value="-1" selected>1</option>
								  <option value="1">2</option>
								  <option value="2">3</option>
								  <option value="3">4</option>
								  <option value="4">5</option>
								</select>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="form-group">
								<label for="exampleFormControlSelect1">Pilih Kecamatan</label>
								<select id="select-kecamatan" class="cd-select">
								  <option value="-1" selected>1</option>
								  <option value="1">2</option>
								  <option value="2">3</option>
								  <option value="3">4</option>
								  <option value="4">5</option>
								</select>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="form-group">
								<label for="exampleFormControlSelect1">Pilih Radius</label>
								<select id="select-radius" class="cd-select">
								  <option value="-1" selected>1</option>
								  <option value="1">2</option>
								  <option value="2">3</option>
								  <option value="3">4</option>
								  <option value="4">5</option>
								</select>
							</div>
						</div>
						<div class="col-sm-9">
							<input id="e13" type="text" data-slider-tooltip="hide" data-slider-value="100" data-slider-ticks="[0, 50, 100, 150, 200, 250, 300]" data-slider-ticks-snap-bounds="50" data-slider-ticks-labels='["0Rb.", "50Rb.", "100Rb.", "150Rb.", "200Rb.", "250Rb.", "300Rb."]'/>

							<script type="text/javascript" charset="utf-8">
							  // With JQuery
								$("#e13").slider({
									ticks: [0, 50, 100, 150, 200, 250, 300],
									ticks_labels: ['0Rb.', '50Rb.', '100Rb.', '150Rb.', '200Rb.', '250Rb.', '300Rb.'],
									ticks_snap_bounds: 50
								});
							</script>
						</div>
						<div class="col-sm-3">
							<a href="#" class="btn btn-primary btn-block" role="button">Search</a>
						</div>
					</div>
				</div>

				<div class="km-news-ticker">
					<p class="text-center"><strong>Nenwsticker :</strong> Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.</p>
				</div>

				<!-- Promo box Content Start -->
				<div class="km-promo-box">
					<div class="md-padding">
						<div class="row">
							<div class="col-sm-12 col-md-6 col-lg-4">
								<div class="km-hot-deals">
									<h3>Hot Deals Makanan Basah</h3>
									<section class="km-hotdeals-items slider">
										<div>
											<img src="{img_path}km-thumbnail-1.jpg"/>
											<a href="#"><h3>Budae Jigae Stew</h3></a>
											<a href="#"><h5>Mujigae Korean Food</h5></a>
											<h3><span class="km-price-cut">Rp.48.000</span> Rp.40.000</h3>
											<a href="#" class="btn btn-primary btn-lg btn-block" role="button"><i class="fa fa-search"></i>Lihat Detail</a>
										</div>
										<div>
											<img src="{img_path}km-thumbnail-2.jpg"/>
											<a href="#"><h3>Mie Bakso + Tulang</h3></a>
											<a href="#"><h5>Bakso Semar</h5></a>
											<h3><span class="km-price-cut">Rp.25.000</span> Rp.15.000</h3>
											<a href="#" class="btn btn-primary btn-lg btn-block" role="button"><i class="fa fa-search"></i>Lihat Detail</a>
										</div>
										<div>
											<img src="{img_path}km-thumbnail-3.jpg"/>
											<a href="#"><h3>Paket Suki + Minum</h3></a>
											<a href="#"><h5>Raacha Suki & BBQ</h5></a>
											<h3><span class="km-price-cut">Rp.48.000</span> Rp.37.000</h3>
											<a href="#" class="btn btn-primary btn-lg btn-block" role="button"><i class="fa fa-search"></i>Lihat Detail</a>
										</div>
									</section>
								</div>
								<img src="{img_path}ads-promobox-1.jpg" class="tb-sm-padding"/>
							</div>
							<div class="col-sm-12 col-md-6 col-lg-8">
								<div class="row b-md-padding">
									<div class="col-sm-6 col-md-6 col-lg-6">
										<h3>Promo makanan basah lainnya</h3>
									</div>
									<div class="col-sm-6 col-md-6 col-lg-6">
										<form class="form-inline my-2 my-lg-0 km-search-snippet">
										  <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
										  <button class="btn btn-primary my-2 my-sm-0" type="submit">Search</button>
										</form>
									</div>
								</div>
									<section class="km-promo-items slider row">
										<div>
											<div class="col-sm-3 col-md-12 col-lg-3 grandchild">
												<img src="{img_path}km-thumbnail-2.jpg" class="max-img-default"/>
												<a href="#"><h4>Mie Bakso + Tulang</h4></a>
												<a href="#"><h5>Bakso Semar</h5></a>
												<h6>Rp.15.000</h6>
												<a href="#" class="btn btn-primary btn-sm btn-block" role="button"><i class="fa fa-search"></i>Lihat Detail</a>
											</div>

											<div class="col-sm-3 col-md-12 col-lg-3 grandchild">
												<img src="{img_path}km-thumbnail-3.jpg" class="max-img-default"/>
												<a href="#"><h4>Paket Suki + Minum</h4></a>
												<a href="#"><h5>Raacha Suki & BBQ</h5></a>
												<h6>Rp.37.000</h6>
												<a href="#" class="btn btn-primary btn-sm btn-block" role="button"><i class="fa fa-search"></i>Lihat Detail</a>
											</div>

											<div class="col-sm-3 col-md-12 col-lg-3 grandchild">
												<img src="{img_path}km-thumbnail-4.jpg" class="max-img-default"/>
												<a href="#"><h4>Bakso Malang Campur</h4></a>
												<a href="#"><h5>Bakso Enggal</h5></a>
												<h6>Rp.25.000</h6>
												<a href="#" class="btn btn-primary btn-sm btn-block" role="button"><i class="fa fa-search"></i>Lihat Detail</a>
											</div>

											<div class="col-sm-3 col-md-12 col-lg-3 grandchild">
												<img src="{img_path}km-thumbnail-5.jpg" class="max-img-default"/>
												<a href="#"><h4>Bakso Malang Campur</h4></a>
												<a href="#"><h5>Gokanna Ramen</h5></a>
												<h6>Rp.35.000</h6>
												<a href="#" class="btn btn-primary btn-sm btn-block" role="button"><i class="fa fa-search"></i>Lihat Detail</a>
											</div>

											<br class="clearboth">

											<div class="col-sm-3 col-md-12 col-lg-3 grandchild">
												<img src="{img_path}km-thumbnail-6.jpg" class="max-img-default"/>
												<a href="#"><h4>Soto Ayam Lamongan</h4></a>
												<a href="#"><h5>Cak Soleh</h5></a>
												<h6>Rp.17.000</h6>
												<a href="#" class="btn btn-primary btn-sm btn-block" role="button"><i class="fa fa-search"></i>Lihat Detail</a>
											</div>

											<div class="col-sm-3 col-md-12 col-lg-3 grandchild">
												<img src="{img_path}km-thumbnail-7.jpg" class="max-img-default"/>
												<a href="#"><h4>Sop Konro Makassar</h4></a>
												<a href="#"><h5>Konro Marannu</h5></a>
												<h6>Rp.43.000</h6>
												<a href="#" class="btn btn-primary btn-sm btn-block" role="button"><i class="fa fa-search"></i>Lihat Detail</a>
											</div>
											<div class="col-sm-3 col-md-12 col-lg-3 grandchild">
												<img src="{img_path}km-thumbnail-8.jpg" class="max-img-default"/>
												<a href="#"><h4>Sop Buntut</h4></a>
												<a href="#"><h5>Sop Buntut Pak Iing</h5></a>
												<h6>Rp.30.000</h6>
												<a href="#" class="btn btn-primary btn-sm btn-block" role="button"><i class="fa fa-search"></i>Lihat Detail</a>
											</div>

											<div class="col-sm-3 col-md-12 col-lg-3 grandchild">
												<img src="{img_path}km-thumbnail-9.jpg" class="max-img-default"/>
												<a href="#"><h4>Es Shanghai</h4></a>
												<a href="#"><h5>Es Shanghai Fadhilah</h5></a>
												<h6>Rp.10.000</h6>
												<a href="#" class="btn btn-primary btn-sm btn-block" role="button"><i class="fa fa-search"></i>Lihat Detail</a>
											</div>

											<br class="clearboth">

											<div class="col-sm-3 col-md-12 col-lg-3 grandchild">
												<img src="{img_path}km-thumbnail-4.jpg" class="max-img-default"/>
												<a href="#"><h4>Bakso Malang Campur</h4></a>
												<a href="#"><h5>Bakso Enggal</h5></a>
												<h6>Rp.25.000</h6>
												<a href="#" class="btn btn-primary btn-sm btn-block" role="button"><i class="fa fa-search"></i>Lihat Detail</a>
											</div>

											<div class="col-sm-3 col-md-12 col-lg-3 grandchild">
												<img src="{img_path}km-thumbnail-2.jpg" class="max-img-default"/>
												<a href="#"><h4>Mie Bakso + Tulang</h4></a>
												<a href="#"><h5>Bakso Semar</h5></a>
												<h6>Rp.15.000</h6>
												<a href="#" class="btn btn-primary btn-sm btn-block" role="button"><i class="fa fa-search"></i>Lihat Detail</a>
											</div>

											<div class="col-sm-3 col-md-12 col-lg-3 grandchild">
												<img src="{img_path}km-thumbnail-5.jpg" class="max-img-default"/>
												<a href="#"><h4>Bakso Malang Campur</h4></a>
												<a href="#"><h5>Gokanna Ramen</h5></a>
												<h6>Rp.35.000</h6>
												<a href="#" class="btn btn-primary btn-sm btn-block" role="button"><i class="fa fa-search"></i>Lihat Detail</a>
											</div>

											<div class="col-sm-3 col-md-12 col-lg-3 grandchild">
												<img src="{img_path}km-thumbnail-3.jpg" class="max-img-default"/>
												<a href="#"><h4>Paket Suki + Minum</h4></a>
												<a href="#"><h5>Raacha Suki & BBQ</h5></a>
												<h6>Rp.37.000</h6>
												<a href="#" class="btn btn-primary btn-sm btn-block" role="button"><i class="fa fa-search"></i>Lihat Detail</a>
											</div>
										</div>

										<div>
											<div class="col-sm-3 col-md-12 col-lg-3 grandchild">
												<img src="{img_path}km-thumbnail-4.jpg" class="max-img-default"/>
												<a href="#"><h4>Bakso Malang Campur</h4></a>
												<a href="#"><h5>Bakso Enggal</h5></a>
												<h6>Rp.25.000</h6>
												<a href="#" class="btn btn-primary btn-block" role="button"><i class="fa fa-search"></i>Lihat Detail</a>
											</div>

											<div class="col-sm-3 col-md-12 col-lg-3 grandchild">
												<img src="{img_path}km-thumbnail-2.jpg" class="max-img-default"/>
												<a href="#"><h4>Mie Bakso + Tulang</h4></a>
												<a href="#"><h5>Bakso Semar</h5></a>
												<h6>Rp.15.000</h6>
												<a href="#" class="btn btn-primary btn-block" role="button"><i class="fa fa-search"></i>Lihat Detail</a>
											</div>

											<div class="col-sm-3 col-md-12 col-lg-3 grandchild">
												<img src="{img_path}km-thumbnail-5.jpg" class="max-img-default"/>
												<a href="#"><h4>Bakso Malang Campur</h4></a>
												<a href="#"><h5>Gokanna Ramen</h5></a>
												<h6>Rp.35.000</h6>
												<a href="#" class="btn btn-primary btn-block" role="button"><i class="fa fa-search"></i>Lihat Detail</a>
											</div>

											<div class="col-sm-3 col-md-12 col-lg-3 grandchild">
												<img src="{img_path}km-thumbnail-3.jpg" class="max-img-default"/>
												<a href="#"><h4>Paket Suki + Minum</h4></a>
												<a href="#"><h5>Raacha Suki & BBQ</h5></a>
												<h6>Rp.37.000</h6>
												<a href="#" class="btn btn-primary btn-block" role="button"><i class="fa fa-search"></i>Lihat Detail</a>
											</div>

											<br class="clearboth">

											<div class="col-sm-3 col-md-12 col-lg-3 grandchild">
												<img src="{img_path}km-thumbnail-7.jpg" class="max-img-default"/>
												<a href="#"><h4>Sop Konro Makassar</h4></a>
												<a href="#"><h5>Konro Marannu</h5></a>
												<h6>Rp.43.000</h6>
												<a href="#" class="btn btn-primary btn-block" role="button"><i class="fa fa-search"></i>Lihat Detail</a>
											</div>

											<div class="col-sm-3 col-md-12 col-lg-3 grandchild">
												<img src="{img_path}km-thumbnail-6.jpg" class="max-img-default"/>
												<a href="#"><h4>Soto Ayam Lamongan</h4></a>
												<a href="#"><h5>Cak Soleh</h5></a>
												<h6>Rp.17.000</h6>
												<a href="#" class="btn btn-primary btn-block" role="button"><i class="fa fa-search"></i>Lihat Detail</a>
											</div>

											<div class="col-sm-3 col-md-12 col-lg-3 grandchild">
												<img src="{img_path}km-thumbnail-9.jpg" class="max-img-default"/>
												<a href="#"><h4>Es Shanghai</h4></a>
												<a href="#"><h5>Es Shanghai Fadhilah</h5></a>
												<h6>Rp.10.000</h6>
												<a href="#" class="btn btn-primary btn-block" role="button"><i class="fa fa-search"></i>Lihat Detail</a>
											</div>

											<div class="col-sm-3 col-md-12 col-lg-3 grandchild">
												<img src="{img_path}km-thumbnail-8.jpg" class="max-img-default"/>
												<a href="#"><h4>Sop Buntut</h4></a>
												<a href="#"><h5>Sop Buntut Pak Iing</h5></a>
												<h6>Rp.30.000</h6>
												<a href="#" class="btn btn-primary btn-block" role="button"><i class="fa fa-search"></i>Lihat Detail</a>
											</div>

											<br class="clearboth">

											<div class="col-sm-3 col-md-12 col-lg-3 grandchild">
												<img src="{img_path}km-thumbnail-4.jpg" class="max-img-default"/>
												<a href="#"><h4>Bakso Malang Campur</h4></a>
												<a href="#"><h5>Bakso Enggal</h5></a>
												<h6>Rp.25.000</h6>
												<a href="#" class="btn btn-primary btn-sm btn-block" role="button"><i class="fa fa-search"></i>Lihat Detail</a>
											</div>

											<div class="col-sm-3 col-md-12 col-lg-3 grandchild">
												<img src="{img_path}km-thumbnail-2.jpg" class="max-img-default"/>
												<a href="#"><h4>Mie Bakso + Tulang</h4></a>
												<a href="#"><h5>Bakso Semar</h5></a>
												<h6>Rp.15.000</h6>
												<a href="#" class="btn btn-primary btn-sm btn-block" role="button"><i class="fa fa-search"></i>Lihat Detail</a>
											</div>

											<div class="col-sm-3 col-md-12 col-lg-3 grandchild">
												<img src="{img_path}km-thumbnail-5.jpg" class="max-img-default"/>
												<a href="#"><h4>Bakso Malang Campur</h4></a>
												<a href="#"><h5>Gokanna Ramen</h5></a>
												<h6>Rp.35.000</h6>
												<a href="#" class="btn btn-primary btn-sm btn-block" role="button"><i class="fa fa-search"></i>Lihat Detail</a>
											</div>

											<div class="col-sm-3 col-md-12 col-lg-3 grandchild">
												<img src="{img_path}km-thumbnail-3.jpg" class="max-img-default"/>
												<a href="#"><h4>Paket Suki + Minum</h4></a>
												<a href="#"><h5>Raacha Suki & BBQ</h5></a>
												<h6>Rp.37.000</h6>
												<a href="#" class="btn btn-primary btn-sm btn-block" role="button"><i class="fa fa-search"></i>Lihat Detail</a>
											</div>
										</div>
									</section>

								<div class="row">
									<div class="col-sm-12 col-md-6 col-lg-6">
										<p>Ada sekitar <strong>345</strong> promo dalam kategori ini</p>
									</div>

								</div>
							</div>
						</div>
					</div>
					<div class="km-partner-box">
						<div class="row">
							<div class="col-sm-12 col-md-3 col-lg-3">
								<h3 class="km-partner-title-box">Partner di kategori ini</h3>
							</div>
							<div class="col-sm-12 col-md-9 col-lg-9">
								<section class="km-partner-items slider">
									<div>
										<a href="#"><img src="{img_path}partner-1.png"/></a>
									</div>
									<div>
										<a href="#"><img src="{img_path}partner-2.png"/></a>
									</div>
									<div>
										<a href="#"><img src="{img_path}partner-3.png"/></a>
									</div>
									<div>
										<a href="#"><img src="{img_path}partner-4.png"/></a>
									</div>
									<div>
										<a href="#"><img src="{img_path}partner-5.png"/></a>
									</div>
									<div>
										<a href="#"><img src="{img_path}partner-6.png"/></a>
									</div>
									<div>
										<a href="#"><img src="{img_path}partner-5.png"/></a>
									</div>
									<div>
										<a href="#"><img src="{img_path}partner-4.png"/></a>
									</div>
									<div>
										<a href="#"><img src="{img_path}partner-3.png"/></a>
									</div>
									<div>
										<a href="#"><img src="{img_path}partner-2.png"/></a>
									</div>
								</section>
							</div>
						</div>
					</div>
				</div>
				<!-- Promo box Content End -->

				<!-- Promo box Content Start -->
				<div class="km-promo-box">
					<div class="md-padding">
						<div class="row">
							<div class="col-sm-12 col-md-6 col-lg-4">
								<div class="km-hot-deals">
									<h3>Hot Deals Makanan Basah</h3>
									<img src="{img_path}km-thumbnail-1.jpg"/>
									<a href="#"><h3>Budae Jigae Stew</h3></a>
									<a href="#"><h5>Mujigae Korean Food</h5></a>
									<h3><span class="km-price-cut">Rp.48.000</span> Rp.40.000</h3>
									<a href="#" class="btn btn-primary btn-lg btn-block" role="button"><i class="fa fa-search"></i>Lihat Detail</a>
								</div>
								<img src="{img_path}ads-promobox-1.jpg" class="tb-sm-padding"/>
							</div>
							<div class="col-sm-12 col-md-6 col-lg-8">
								<div class="row b-sm-padding">
									<div class="col-sm-6 col-md-6 col-lg-6">
										<h3>Promo makanan kering lainnya</h3>
									</div>
									<div class="col-sm-6 col-md-6 col-lg-6">
										<form class="form-inline my-2 my-lg-0">
										  <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
										  <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
										</form>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-3 col-md-12 col-lg-3">
										<img src="{img_path}km-thumbnail-2.jpg" class="max-img-default"/>
										<a href="#"><h4>Mie Bakso + Tulang</h4></a>
										<a href="#"><h5>Bakso Semar</h5></a>
										<h6>Rp.15.000</h6>
										<a href="#" class="btn btn-primary btn-block" role="button"><i class="fa fa-search"></i>Lihat Detail</a>
									</div>
									<div class="col-sm-3 col-md-12 col-lg-3">
										<img src="{img_path}km-thumbnail-3.jpg" class="max-img-default"/>
										<a href="#"><h4>Paket Suki + Minum</h4></a>
										<a href="#"><h5>Raacha Suki & BBQ</h5></a>
										<h6>Rp.37.000</h6>
										<a href="#" class="btn btn-primary btn-block" role="button"><i class="fa fa-search"></i>Lihat Detail</a>
									</div>
									<div class="col-sm-3 col-md-12 col-lg-3">
										<img src="{img_path}km-thumbnail-4.jpg" class="max-img-default"/>
										<a href="#"><h4>Bakso Malang Campur</h4></a>
										<a href="#"><h5>Bakso Enggal</h5></a>
										<h6>Rp.25.000</h6>
										<a href="#" class="btn btn-primary btn-block" role="button"><i class="fa fa-search"></i>Lihat Detail</a>
									</div>
									<div class="col-sm-3 col-md-12 col-lg-3">
										<img src="{img_path}km-thumbnail-5.jpg" class="max-img-default"/>
										<a href="#"><h4>Bakso Malang Campur</h4></a>
										<a href="#"><h5>Gokanna Ramen</h5></a>
										<h6>Rp.35.000</h6>
										<a href="#" class="btn btn-primary btn-block" role="button"><i class="fa fa-search"></i>Lihat Detail</a>
									</div>
									<div class="col-sm-3 col-md-12 col-lg-3">
										<img src="{img_path}km-thumbnail-6.jpg" class="max-img-default"/>
										<a href="#"><h4>Soto Ayam Lamongan</h4></a>
										<a href="#"><h5>Cak Soleh</h5></a>
										<h6>Rp.17.000</h6>
										<a href="#" class="btn btn-primary btn-block" role="button"><i class="fa fa-search"></i>Lihat Detail</a>
									</div>
									<div class="col-sm-3 col-md-12 col-lg-3">
										<img src="{img_path}km-thumbnail-7.jpg" class="max-img-default"/>
										<a href="#"><h4>Sop Konro Makassar</h4></a>
										<a href="#"><h5>Konro Marannu</h5></a>
										<h6>Rp.43.000</h6>
										<a href="#" class="btn btn-primary btn-block" role="button"><i class="fa fa-search"></i>Lihat Detail</a>
									</div>
									<div class="col-sm-3 col-md-12 col-lg-3">
										<img src="{img_path}km-thumbnail-8.jpg" class="max-img-default"/>
										<a href="#"><h4>Sop Buntut</h4></a>
										<a href="#"><h5>Sop Buntut Pak Iing</h5></a>
										<h6>Rp.30.000</h6>
										<a href="#" class="btn btn-primary btn-block" role="button"><i class="fa fa-search"></i>Lihat Detail</a>
									</div>
									<div class="col-sm-3 col-md-12 col-lg-3">
										<img src="{img_path}km-thumbnail-9.jpg" class="max-img-default"/>
										<a href="#"><h4>Es Shanghai</h4></a>
										<a href="#"><h5>Es Shanghai Fadhilah</h5></a>
										<h6>Rp.10.000</h6>
										<a href="#" class="btn btn-primary btn-block" role="button"><i class="fa fa-search"></i>Lihat Detail</a>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12 col-md-6 col-lg-6">
										<p>Ada sekitar <strong>345</strong> promo dalam kategori ini</p>
									</div>
									<div class="col-sm-12 col-md-6 col-lg-6">
										<p class="text-right">Slider here</p>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="km-partner-box">
						<div class="row">
							<div class="col-sm-12 col-md-3 col-lg-3">
								<h3 class="km-partner-title-box">Partner di kategori ini</h3>
							</div>
							<div class="col-sm-12 col-md-9 col-lg-9">
								<section class="km-partner-items slider">
									<div>
										<a href="#"><img src="{img_path}partner-1.png"/></a>
									</div>
									<div>
										<a href="#"><img src="{img_path}partner-2.png"/></a>
									</div>
									<div>
										<a href="#"><img src="{img_path}partner-3.png"/></a>
									</div>
									<div>
										<a href="#"><img src="{img_path}partner-4.png"/></a>
									</div>
									<div>
										<a href="#"><img src="{img_path}partner-5.png"/></a>
									</div>
									<div>
										<a href="#"><img src="{img_path}partner-6.png"/></a>
									</div>
									<div>
										<a href="#"><img src="{img_path}partner-5.png"/></a>
									</div>
									<div>
										<a href="#"><img src="{img_path}partner-4.png"/></a>
									</div>
									<div>
										<a href="#"><img src="{img_path}partner-3.png"/></a>
									</div>
									<div>
										<a href="#"><img src="{img_path}partner-2.png"/></a>
									</div>
								</section>
							</div>
						</div>
					</div>
				</div>
				<!-- Promo box Content End -->

				<!-- Promo box Content Start -->
				<div class="km-promo-box">
					<div class="md-padding">
						<div class="row">
							<div class="col-sm-12 col-md-6 col-lg-4">
								<div class="km-hot-deals">
									<h3>Hot Deals Makanan Jadi</h3>
									<img src="{img_path}km-thumbnail-1.jpg"/>
									<a href="#"><h3>Budae Jigae Stew</h3></a>
									<a href="#"><h5>Mujigae Korean Food</h5></a>
									<h3><span class="km-price-cut">Rp.48.000</span> Rp.40.000</h3>
									<a href="#" class="btn btn-primary btn-lg btn-block" role="button"><i class="fa fa-search"></i>Lihat Detail</a>
								</div>
								<img src="{img_path}ads-promobox-1.jpg" class="tb-sm-padding"/>
							</div>
							<div class="col-sm-12 col-md-6 col-lg-8">
								<div class="row b-sm-padding">
									<div class="col-sm-6 col-md-6 col-lg-6">
										<h3>Promo makanan jadi lainnya</h3>
									</div>
									<div class="col-sm-6 col-md-6 col-lg-6">
										<form class="form-inline my-2 my-lg-0">
										  <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
										  <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
										</form>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-3 col-md-12 col-lg-3">
										<img src="{img_path}km-thumbnail-2.jpg" class="max-img-default"/>
										<a href="#"><h4>Mie Bakso + Tulang</h4></a>
										<a href="#"><h5>Bakso Semar</h5></a>
										<h6>Rp.15.000</h6>
										<a href="#" class="btn btn-primary btn-block" role="button"><i class="fa fa-search"></i>Lihat Detail</a>
									</div>
									<div class="col-sm-3 col-md-12 col-lg-3">
										<img src="{img_path}km-thumbnail-3.jpg" class="max-img-default"/>
										<a href="#"><h4>Paket Suki + Minum</h4></a>
										<a href="#"><h5>Raacha Suki & BBQ</h5></a>
										<h6>Rp.37.000</h6>
										<a href="#" class="btn btn-primary btn-block" role="button"><i class="fa fa-search"></i>Lihat Detail</a>
									</div>
									<div class="col-sm-3 col-md-12 col-lg-3">
										<img src="{img_path}km-thumbnail-4.jpg" class="max-img-default"/>
										<a href="#"><h4>Bakso Malang Campur</h4></a>
										<a href="#"><h5>Bakso Enggal</h5></a>
										<h6>Rp.25.000</h6>
										<a href="#" class="btn btn-primary btn-block" role="button"><i class="fa fa-search"></i>Lihat Detail</a>
									</div>
									<div class="col-sm-3 col-md-12 col-lg-3">
										<img src="{img_path}km-thumbnail-5.jpg" class="max-img-default"/>
										<a href="#"><h4>Bakso Malang Campur</h4></a>
										<a href="#"><h5>Gokanna Ramen</h5></a>
										<h6>Rp.35.000</h6>
										<a href="#" class="btn btn-primary btn-block" role="button"><i class="fa fa-search"></i>Lihat Detail</a>
									</div>
									<div class="col-sm-3 col-md-12 col-lg-3">
										<img src="{img_path}km-thumbnail-6.jpg" class="max-img-default"/>
										<a href="#"><h4>Soto Ayam Lamongan</h4></a>
										<a href="#"><h5>Cak Soleh</h5></a>
										<h6>Rp.17.000</h6>
										<a href="#" class="btn btn-primary btn-block" role="button"><i class="fa fa-search"></i>Lihat Detail</a>
									</div>
									<div class="col-sm-3 col-md-12 col-lg-3">
										<img src="{img_path}km-thumbnail-7.jpg" class="max-img-default"/>
										<a href="#"><h4>Sop Konro Makassar</h4></a>
										<a href="#"><h5>Konro Marannu</h5></a>
										<h6>Rp.43.000</h6>
										<a href="#" class="btn btn-primary btn-block" role="button"><i class="fa fa-search"></i>Lihat Detail</a>
									</div>
									<div class="col-sm-3 col-md-12 col-lg-3">
										<img src="{img_path}km-thumbnail-8.jpg" class="max-img-default"/>
										<a href="#"><h4>Sop Buntut</h4></a>
										<a href="#"><h5>Sop Buntut Pak Iing</h5></a>
										<h6>Rp.30.000</h6>
										<a href="#" class="btn btn-primary btn-block" role="button"><i class="fa fa-search"></i>Lihat Detail</a>
									</div>
									<div class="col-sm-3 col-md-12 col-lg-3">
										<img src="{img_path}km-thumbnail-9.jpg" class="max-img-default"/>
										<a href="#"><h4>Es Shanghai</h4></a>
										<a href="#"><h5>Es Shanghai Fadhilah</h5></a>
										<h6>Rp.10.000</h6>
										<a href="#" class="btn btn-primary btn-block" role="button"><i class="fa fa-search"></i>Lihat Detail</a>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12 col-md-6 col-lg-6">
										<p>Ada sekitar <strong>345</strong> promo dalam kategori ini</p>
									</div>
									<div class="col-sm-12 col-md-6 col-lg-6">
										<p class="text-right">Slider here</p>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="km-partner-box">
						<div class="row">
							<div class="col-sm-12 col-md-3 col-lg-3">
								<h3 class="km-partner-title-box">Partner di kategori ini</h3>
							</div>
							<div class="col-sm-12 col-md-9 col-lg-9">
								<section class="km-partner-items slider">
									<div>
										<a href="#"><img src="{img_path}partner-1.png"/></a>
									</div>
									<div>
										<a href="#"><img src="{img_path}partner-2.png"/></a>
									</div>
									<div>
										<a href="#"><img src="{img_path}partner-3.png"/></a>
									</div>
									<div>
										<a href="#"><img src="{img_path}partner-4.png"/></a>
									</div>
									<div>
										<a href="#"><img src="{img_path}partner-5.png"/></a>
									</div>
									<div>
										<a href="#"><img src="{img_path}partner-6.png"/></a>
									</div>
									<div>
										<a href="#"><img src="{img_path}partner-5.png"/></a>
									</div>
									<div>
										<a href="#"><img src="{img_path}partner-4.png"/></a>
									</div>
									<div>
										<a href="#"><img src="{img_path}partner-3.png"/></a>
									</div>
									<div>
										<a href="#"><img src="{img_path}partner-2.png"/></a>
									</div>
								</section>
							</div>
						</div>
					</div>
				</div>
				<!-- Promo box Content End -->

				<!-- Promo box Content Start -->
				<div class="km-promo-box">
					<div class="md-padding">
						<div class="row">
							<div class="col-sm-12 col-md-6 col-lg-4">
								<div class="km-hot-deals">
									<h3>Hot Deals Makanan Setengah Jadi</h3>
									<img src="{img_path}km-thumbnail-1.jpg"/>
									<a href="#"><h3>Budae Jigae Stew</h3></a>
									<a href="#"><h5>Mujigae Korean Food</h5></a>
									<h3><span class="km-price-cut">Rp.48.000</span> Rp.40.000</h3>
									<a href="#" class="btn btn-primary btn-lg btn-block" role="button"><i class="fa fa-search"></i>Lihat Detail</a>
								</div>
								<img src="{img_path}ads-promobox-1.jpg" class="tb-sm-padding"/>
							</div>
							<div class="col-sm-12 col-md-6 col-lg-8">
								<div class="row b-sm-padding">
									<div class="col-sm-6 col-md-6 col-lg-6">
										<h3>Promo makanan setengah jadi lainnya</h3>
									</div>
									<div class="col-sm-6 col-md-6 col-lg-6">
										<form class="form-inline my-2 my-lg-0">
										  <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
										  <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
										</form>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-3 col-md-12 col-lg-3">
										<img src="{img_path}km-thumbnail-2.jpg" class="max-img-default"/>
										<a href="#"><h4>Mie Bakso + Tulang</h4></a>
										<a href="#"><h5>Bakso Semar</h5></a>
										<h6>Rp.15.000</h6>
										<a href="#" class="btn btn-primary btn-block" role="button"><i class="fa fa-search"></i>Lihat Detail</a>
									</div>
									<div class="col-sm-3 col-md-12 col-lg-3">
										<img src="{img_path}km-thumbnail-3.jpg" class="max-img-default"/>
										<a href="#"><h4>Paket Suki + Minum</h4></a>
										<a href="#"><h5>Raacha Suki & BBQ</h5></a>
										<h6>Rp.37.000</h6>
										<a href="#" class="btn btn-primary btn-block" role="button"><i class="fa fa-search"></i>Lihat Detail</a>
									</div>
									<div class="col-sm-3 col-md-12 col-lg-3">
										<img src="{img_path}km-thumbnail-4.jpg" class="max-img-default"/>
										<a href="#"><h4>Bakso Malang Campur</h4></a>
										<a href="#"><h5>Bakso Enggal</h5></a>
										<h6>Rp.25.000</h6>
										<a href="#" class="btn btn-primary btn-block" role="button"><i class="fa fa-search"></i>Lihat Detail</a>
									</div>
									<div class="col-sm-3 col-md-12 col-lg-3">
										<img src="{img_path}km-thumbnail-5.jpg" class="max-img-default"/>
										<a href="#"><h4>Bakso Malang Campur</h4></a>
										<a href="#"><h5>Gokanna Ramen</h5></a>
										<h6>Rp.35.000</h6>
										<a href="#" class="btn btn-primary btn-block" role="button"><i class="fa fa-search"></i>Lihat Detail</a>
									</div>
									<div class="col-sm-3 col-md-12 col-lg-3">
										<img src="{img_path}km-thumbnail-6.jpg" class="max-img-default"/>
										<a href="#"><h4>Soto Ayam Lamongan</h4></a>
										<a href="#"><h5>Cak Soleh</h5></a>
										<h6>Rp.17.000</h6>
										<a href="#" class="btn btn-primary btn-block" role="button"><i class="fa fa-search"></i>Lihat Detail</a>
									</div>
									<div class="col-sm-3 col-md-12 col-lg-3">
										<img src="{img_path}km-thumbnail-7.jpg" class="max-img-default"/>
										<a href="#"><h4>Sop Konro Makassar</h4></a>
										<a href="#"><h5>Konro Marannu</h5></a>
										<h6>Rp.43.000</h6>
										<a href="#" class="btn btn-primary btn-block" role="button"><i class="fa fa-search"></i>Lihat Detail</a>
									</div>
									<div class="col-sm-3 col-md-12 col-lg-3">
										<img src="{img_path}km-thumbnail-8.jpg" class="max-img-default"/>
										<a href="#"><h4>Sop Buntut</h4></a>
										<a href="#"><h5>Sop Buntut Pak Iing</h5></a>
										<h6>Rp.30.000</h6>
										<a href="#" class="btn btn-primary btn-block" role="button"><i class="fa fa-search"></i>Lihat Detail</a>
									</div>
									<div class="col-sm-3 col-md-12 col-lg-3">
										<img src="{img_path}km-thumbnail-9.jpg" class="max-img-default"/>
										<a href="#"><h4>Es Shanghai</h4></a>
										<a href="#"><h5>Es Shanghai Fadhilah</h5></a>
										<h6>Rp.10.000</h6>
										<a href="#" class="btn btn-primary btn-block" role="button"><i class="fa fa-search"></i>Lihat Detail</a>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12 col-md-6 col-lg-6">
										<p>Ada sekitar <strong>345</strong> promo dalam kategori ini</p>
									</div>
									<div class="col-sm-12 col-md-6 col-lg-6">
										<p class="text-right">Slider here</p>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="km-partner-box">
						<div class="row">
							<div class="col-sm-12 col-md-3 col-lg-3">
								<h3 class="km-partner-title-box">Partner di kategori ini</h3>
							</div>
							<div class="col-sm-12 col-md-9 col-lg-9">
								<section class="km-partner-items slider">
									<div>
										<a href="#"><img src="{img_path}partner-1.png"/></a>
									</div>
									<div>
										<a href="#"><img src="{img_path}partner-2.png"/></a>
									</div>
									<div>
										<a href="#"><img src="{img_path}partner-3.png"/></a>
									</div>
									<div>
										<a href="#"><img src="{img_path}partner-4.png"/></a>
									</div>
									<div>
										<a href="#"><img src="{img_path}partner-5.png"/></a>
									</div>
									<div>
										<a href="#"><img src="{img_path}partner-6.png"/></a>
									</div>
									<div>
										<a href="#"><img src="{img_path}partner-5.png"/></a>
									</div>
									<div>
										<a href="#"><img src="{img_path}partner-4.png"/></a>
									</div>
									<div>
										<a href="#"><img src="{img_path}partner-3.png"/></a>
									</div>
									<div>
										<a href="#"><img src="{img_path}partner-2.png"/></a>
									</div>
								</section>
							</div>
						</div>
					</div>
				</div>
				<!-- Promo box Content End -->

				<!-- Ads Words Start -->
				<div class="km-ads-words">
					<div class="row">
						<div class="col-sm-6 col-md-4 col-lg-2">
							<h5>Judul Iklan 1</h5>
							<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.</p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-2">
							<h5>Judul Iklan 2</h5>
							<p>In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus.</p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-2">
							<h5>Judul Iklan 3</h5>
							<p>Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat</p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-2">
							<h5>Judul Iklan 4</h5>
							<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.</p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-2">
							<h5>Judul Iklan 5</h5>
							<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.</p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-2">
							<h5>Judul Iklan 6</h5>
							<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.</p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-2">
							<h5>Judul Iklan 7</h5>
							<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.</p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-2">
							<h5>Judul Iklan 8</h5>
							<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.</p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-2">
							<h5>Judul Iklan 9</h5>
							<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.</p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-2">
							<h5>Judul Iklan 10</h5>
							<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.</p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-2">
							<h5>Judul Iklan 11</h5>
							<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.</p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-2">
							<h5>Judul Iklan 12</h5>
							<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.</p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-2">
							<h5>Judul Iklan 13</h5>
							<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.</p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-2">
							<h5>Judul Iklan 14</h5>
							<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.</p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-2">
							<h5>Judul Iklan 15</h5>
							<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.</p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-2">
							<h5>Judul Iklan 16</h5>
							<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.</p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-2">
							<h5>Judul Iklan 17</h5>
							<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.</p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-2">
							<h5>Judul Iklan 18</h5>
							<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.</p>
						</div>
					</div>
				</div>
				<!-- Ads Words End -->

				<!-- Why Kulcimart Start -->
				<div class="km-whykulci">
					<div class="row">
						<div class="col-sm-12 col-md-12 col-lg-12">
							<h2 class="text-center">Kenapa harus Kulcimart ?</h2>
						</div>
						<div class="col-sm-12 col-md-6 col-lg-3">
							<div class="km-whykulci-block">
								<img src="{img_path}whykulci-1.png"/>
								<h5>Lorem ipsum dolor</h5>
								<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit Aenean commodo ligula eget</p>
							</div>
						</div>
						<div class="col-sm-12 col-md-6 col-lg-3">
							<div class="km-whykulci-block">
								<img src="{img_path}whykulci-2.png"/>
								<h5>Integer tinciduntc ras</h5>
								<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit Aenean commodo ligula eget</p>
							</div>
						</div>
						<div class="col-sm-12 col-md-6 col-lg-3">
							<div class="km-whykulci-block">
								<img src="{img_path}whykulci-3.png"/>
								<h5>Maecenas nec odio et</h5>
								<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit Aenean commodo ligula eget</p>
							</div>
						</div>
						<div class="col-sm-12 col-md-6 col-lg-3">
							<div class="km-whykulci-block">
								<img src="{img_path}whykulci-4.png"/>
								<h5>Etiam sit amet orci</h5>
								<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit Aenean commodo ligula eget</p>
							</div>
						</div>
					</div>
				</div>
				<!-- Why Kulcimart End -->
			</div>

			<!-- Partner Block Section Start -->
			<div class="km-partner-block">
				<div class="container">
					<div class="row">
						<div class="col-sm-12 col-md-12 col-lg-12">
							<h2 class="text-center">Kenapa harus Kulcimart ?</h2>
						</div>
						<div class="col-sm-12 col-md-6 col-lg-6">
							<h4>Partner Favorit</h4>
							<ul class="km-partner-list">
								<li><a href="#"><img src="{img_path}km-partner-1.png"/></a></li>
								<li><a href="#"><img src="{img_path}km-partner-2.png"/></a></li>
								<li><a href="#"><img src="{img_path}km-partner-3.png"/></a></li>
								<li><a href="#"><img src="{img_path}km-partner-4.png"/></a></li>
								<li><a href="#"><img src="{img_path}km-partner-5.png"/></a></li>
								<li><a href="#"><img src="{img_path}km-partner-6.png"/></a></li>
							</ul>
						</div>
						<div class="col-sm-12 col-md-6 col-lg-6">
							<h4>Partner Terbaru</h4>
							<ul class="km-partner-list">
								<li><a href="#"><img src="{img_path}km-partner-7.png"/></a></li>
								<li><a href="#"><img src="{img_path}km-partner-8.png"/></a></li>
								<li><a href="#"><img src="{img_path}km-partner-9.png"/></a></li>
								<li><a href="#"><img src="{img_path}km-partner-10.png"/></a></li>
								<li><a href="#"><img src="{img_path}km-partner-11.png"/></a></li>
								<li><a href="#"><img src="{img_path}km-partner-12.png"/></a></li>
							</ul>
						</div>
						<div class="col-sm-12 col-md-4 col-lg-4">
							<h4>Tunggu Apalagi Daftar Sekarang juga & Jadi bagian KulciMart</h4>
							<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium </p>
							<button class="btn btn-secondary btn-lg" type="submit"><i class="fa fa-user-plus"></i>Daftar Sekarang</button>
						</div>
						<div class="col-sm-12 col-md-8 col-lg-8">
							<h5>Beberapa keuntungan jadi member Kulcimart</h5>
							<ul class="km-list-check">
								<li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget</li>
								<li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget</li>
								<li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget</li>
								<li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget</li>
								<li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget</li>
								<li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget</li>
								<li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget</li>
								<li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget</li>
							</ul>
							<button class="btn btn-secondary btn-lg" type="submit"><i class="fa fa-download"></i>Download Manual</button>
						</div>
					</div>
				</div>
			</div>
			<!-- Partner Block Section End -->

			<!-- Member Block Section Start -->
			<div class="km-member-block">
				<div class="container">
					<div class="row">
						<div class="col-sm-12 col-md-12 col-lg-12">
							<h2 class="text-center">Member Terbaru</h2>
						</div>
						<div class="km-member-list">
							<div class="row">
								<div class="col-sm-12 col-md-2">
									<img src="{img_path}km-member-1.jpg"/>
									<h5>Chelsea Islan</h5>
									<h6>16 Desember 2016</h6>
								</div>
								<div class="col-sm-12 col-md-2">
									<img src="{img_path}km-member-2.jpg"/>
									<h5>Rio Dewanto</h5>
									<h6>17 Desember 2016</h6>
								</div>
								<div class="col-sm-12 col-md-2">
									<img src="{img_path}km-member-3.jpg"/>
									<h5>Kimberly Ryder</h5>
									<h6>18 Desember 2016</h6>
								</div>
								<div class="col-sm-12 col-md-2">
									<img src="{img_path}km-member-4.jpg"/>
									<h5>Dude Herlino</h5>
									<h6>19 Desember 2016</h6>
								</div>
								<div class="col-sm-12 col-md-2">
									<img src="{img_path}km-member-5.jpg"/>
									<h5>Iko Uwais</h5>
									<h6>20 Desember 2016</h6>
								</div>
								<div class="col-sm-12 col-md-2">
									<img src="{img_path}km-member-6.jpg"/>
									<h5>Nikita Willy</h5>
									<h6>21 Desember 2016</h6>
								</div>
							</div>
						</div>
						<div class="col-sm-12 col-md-4 col-lg-4">
							<h4>Tunggu Apalagi Daftar Sekarang juga & Jadi bagian KulciMart</h4>
							<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium </p>
							<button class="btn btn-primary btn-lg" type="submit"><i class="fa fa-user-plus"></i>Daftar Sekarang</button>
						</div>
						<div class="col-sm-12 col-md-8 col-lg-8">
							<h5>Beberapa keuntungan jadi member Kulcimart</h5>
							<ul class="km-list-check">
								<li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget</li>
								<li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget</li>
								<li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget</li>
								<li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget</li>
								<li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget</li>
								<li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget</li>
								<li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget</li>
								<li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget</li>
							</ul>
							<button class="btn btn-primary btn-lg" type="submit"><i class="fa fa-download"></i>Download Manual</button>
						</div>
					</div>
				</div>
			</div>
			<!-- Member Block Section End -->
		</section>

		<!-- Footer Section Start -->
		<section class="footer">
			<div class="container">
				<div class="row">
					<div class="col-sm-12 col-md-12 col-lg-12 text-center">
						<img src="{img_path}header-logo.png" class="b-md-padding"/>
					</div>
					<div class="col-sm-12 col-md-4 col-lg-4">
						<h4>Tentang Kulcimart</h4>
						<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. </p>
						<h4>Terhubung dengan kami</h4>
						<ul class="list-inline">
							<li><a href="#" class="btn btn-primary"><i class="fa fa-facebook"></i>@kulcimart</a></li>
							<li><a href="#" class="btn btn-info"><i class="fa fa-twitter"></i>@kulcimart</a></li>
						</ul>
					</div>

					<div class="col-sm-12 col-md-4 col-lg-4">
						<h4>Kontak kami</h4>
						<h6>Alamat Kantor :</h6>
						<p>Jl. Sukamulya No.45 Bandung</p>
						<h6>Email :</h6>
						<p>info@kurvasoft.com</p>
						<h6>Phone :</h6>
						<p>+6222-2021789</p>
						<h6>Mobile/Whatsapp :</h6>
						<p>+628170278075</p>
					</div>

					<div class="col-sm-12 col-md-4 col-lg-4">
						<h4>Links</h4>
						<ul class="km-footer-link">
							<li><a href="#">Tentang kami</a></li>
							<li><a href="#">Berita</a></li>
							<li><a href="#">FAQ</a></li>
							<li><a href="#">Gallery</a></li>
							<li><a href="#">Event</a></li>
							<li><a href="#">Download</a></li>
						</ul>
						<a href="#"><img src="{img_path}google-badge.png"/></a>
						<p>Copyright &copy; AlumniMatch 2017</p>
					</div>
				</div>
			</div>
		</section>
		<!-- Footer Section End -->
	<script src="{js_path}jquery.canvasjs.min.js"></script>

  <!-- @error -->
	<script src="{js_path}jquery.selectbox.js"></script>

	<script src="{js_path}km-custom-plugin/item-slider/slick.js" type="text/javascript" charset="utf-8"></script>

	<!-- Jquery Item Slider -->
	 <script type="text/javascript">
		$(document).on('ready', function() {
		  $(".km-promo-items").slick({
			autoplay:true,
			infinite: true,
			slidesToShow: 1,
			slidesToScroll: 1
		  });
		});
		$(document).on('ready', function() {
		  $(".km-partner-items").slick({
			autoplay:true,
			slidesToShow: 5,
			slidesToScroll: 2,
			variableWidth: true
		  });
		});
		$(document).on('ready', function() {
		  $(".km-hotdeals-items").slick({
			autoplay:true,
			slidesToShow: 1,
			slidesToScroll: 1,
			variableWidth: true
		  });
		});
	</script>

	<!-- Custom Select Box Plugin Start -->
	<script type="text/javascript">
		$( function() {
			$( '#select-city' ).dropdown(
			{stack : false});
		});
		$( function() {
			$( '#select-kecamatan' ).dropdown(
			{stack : false});
		});
		$( function() {
			$( '#select-radius' ).dropdown(
			{stack : false});
		});
	</script>
	<!-- Custom Select Box Plugin End -->

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->


    <script src="{js_path}bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>
